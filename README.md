# bash-scripting
Bash-scripting repository

This repository represents my configuration for Unix-Like (or Linux) systems through the execution of bash scripts.

```sh
$ git clone "https://github.com/vscalcione/bash-scripting.git"
$ cd bash-scripting
$ sudo chmod +x ${scriptFileName}.sh
$ /.${scriptFileName}.sh
```
